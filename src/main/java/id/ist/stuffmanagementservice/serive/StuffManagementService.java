package id.ist.stuffmanagementservice.serive;

import id.ist.stuffmanagementservice.domain.Employee;
import id.ist.stuffmanagementservice.domain.Pembeli;
import id.ist.stuffmanagementservice.domain.Product;
import id.ist.stuffmanagementservice.dto.ResponseStatus;
import id.ist.stuffmanagementservice.dto.product.*;
import id.ist.stuffmanagementservice.repository.EmployeeRepository;
import id.ist.stuffmanagementservice.repository.PembeliRepository;
import id.ist.stuffmanagementservice.repository.ProductRepository;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Log4j2
@Service
public class StuffManagementService {

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private PembeliRepository pembeliRepository;

    public static Date dateCurrentDate() {
        TimeZone timeZone = TimeZone.getTimeZone("Asia/Jakarta");
        Calendar calendar = Calendar.getInstance(timeZone);
        Date date = calendar.getTime();
        return date;
    }

    public ResponseStatus saveOrUpdateProduct(ProductDto requestInsetDto) {

        try {

            Product data = productRepository.getById(requestInsetDto.getId());
            log.info("### implementTaskRequest : " + data);

            String type = "";
            if (data != null) {

//                if (requestInsetDto.getAmount().equals("null")) {
//                    data.setAmount(data.getAmount() + requestInsetDto.getAmount());
//                }

                if (requestInsetDto.getAmount() != null) {
                    data.setAmount(requestInsetDto.getAmount());
                }

                if (requestInsetDto.getName() != null) {
                    data.setNameProduct(requestInsetDto.getName());
                }
                if (requestInsetDto.getPrice() != null) {
                    data.setPriceProduct(requestInsetDto.getPrice());
                }
                data.setModifiedDate(dateCurrentDate());

                type = "updated";
            }
//            else {
//                data = new Product();
//                Long regexId = Long.valueOf(requestInsetDto.getId().replace("A", ""));
//                Long nexvalId = regexId + 1;
//                String id = "A".concat(String.valueOf(nexvalId));
//
//                data.setId(id);
//                data.setNameProduct(requestInsetDto.getName());
//                data.setAmount(requestInsetDto.getAmount());
//                data.setPriceProduct(requestInsetDto.getPrice());
//                data.setCreatedDate(dateCurrentDate());
//
//                type = "save";
//
//            }
            productRepository.save(data);
            return ResponseStatus.builder()
                    .responseCode("00")
                    .responseMessage("Success")
                    .content(type)
                    .build();

        } catch (Exception e) {
            log.error("Exception saveOrUpdateDetailPermintaan : ", e);
            return ResponseStatus.builder()
                    .responseCode("01")
                    .responseMessage("Error saveOrUpdateProduct")
                    .build();
        }
    }
    public ResponseStatus saveOrUpdateProductv1(ListDataProductDto requestInsetDto) {

        try {

            String type = "updated";
            List<Product> productList = new ArrayList<>();
            for (ProductDto data : requestInsetDto.getProduct()) {
                log.info("### Id product : " + data.getId());

                Product product = productRepository.getById(data.getId());
                log.info("### response from db : " + product);

                if (product != null) {

//                if (requestInsetDto.getAmount().equals("null")) {
//                    data.setAmount(data.getAmount() + requestInsetDto.getAmount());
//                }

                    if (data.getAmount() != null) {
                        product.setAmount(data.getAmount());
                    }

                    if (data.getName() != null) {
                        product.setNameProduct(data.getName());
                    }
                    if (data.getPrice() != null) {
                        product.setPriceProduct(data.getPrice());
                    }
                    product.setModifiedDate(dateCurrentDate());

//                    type = "updated";
                    productList.add(product);
                }
            }

//            else {
//                data = new Product();
//                Long regexId = Long.valueOf(requestInsetDto.getId().replace("A", ""));
//                Long nexvalId = regexId + 1;
//                String id = "A".concat(String.valueOf(nexvalId));
//
//                data.setId(id);
//                data.setNameProduct(requestInsetDto.getName());
//                data.setAmount(requestInsetDto.getAmount());
//                data.setPriceProduct(requestInsetDto.getPrice());
//                data.setCreatedDate(dateCurrentDate());
//
//                type = "save";
//
//            }
            productRepository.saveAll(productList);
            return ResponseStatus.builder()
                    .responseCode("00")
                    .responseMessage("Success")
                    .content(type)
                    .build();

        } catch (Exception e) {
            log.error("Exception saveOrUpdateDetailPermintaan : ", e);
            return ResponseStatus.builder()
                    .responseCode("01")
                    .responseMessage("Error saveOrUpdateProduct")
                    .build();
        }
    }

    public ResponseStatus InsertProduct(ListDataProductDto requestInsetDto) {

        log.info("###request Insert Product : {} " + requestInsetDto);

        try {
            String type = "Insert Product";

            List<Product> productList = new ArrayList<>();
            for (ProductDto data : requestInsetDto.getProduct()) {
                if (data == null || data.getName() == null) {
                    return ResponseStatus.builder()
                            .responseCode("01")
                            .responseMessage("name product is blank")
                            .build();
                }

                Product product = new Product();
                product.setNameProduct(data.getName());
                product.setAmount(data.getAmount() == null ? 0 : data.getAmount());
                product.setPriceProduct(data.getPrice() == null ? 0 : data.getPrice());
                product.setCreatedDate(dateCurrentDate());
                productList.add(product);
            }

            productRepository.saveAll(productList);
            return ResponseStatus.builder()
                    .responseCode("00")
                    .responseMessage("Success")
                    .content(type)
                    .build();

        } catch (Exception e) {
            log.error("Exception saveOrUpdateDetailPermintaan : ", e);
            return ResponseStatus.builder()
                    .responseCode("01")
                    .responseMessage("Error InsertProduct")
                    .build();
        }
    }

    public ResponseStatus InsertAmountProduct(ProductDto requestInsetDto) {

        Product data = productRepository.getById(requestInsetDto.getId());
        log.info("###Data : " + data);
        String type = "updated";

        if (data == null || data.getAmount() == null) {

            return ResponseStatus.builder()
                    .responseCode("01")
                    .responseMessage("amount product is blank")
                    .build();
        }
        data.setAmount(data.getAmount() + requestInsetDto.getAmount());
        data.setModifiedDate(dateCurrentDate());
        productRepository.save(data);
        return ResponseStatus.builder()
                .responseCode("00")
                .responseMessage("Success")
                .content(type)
                .build();
    }

    public ViewPaginationProduct getListProduct(RequestPaginationProduct requestPagination) {

        Pageable pageable = PageRequest.of(requestPagination.getPageNumber(), requestPagination.getDataPerPage(),
                "DESC".equals(requestPagination.getSortType()) ? Sort.Direction.DESC:Sort.Direction.ASC, requestPagination.getSortBy());

        List<ProductDto> list = new ArrayList<>();
        Page<Product> products = productRepository.findAll(pageable);

        List<Product> productList = products.getContent();

        for (Product data : productList) {
            ProductDto productDto = ProductDto.builder()
                    .id(data.getId())
                    .name(data.getNameProduct())
                    .amount(data.getAmount())
                    .price(data.getPriceProduct())
                    .build();

            list.add(productDto);
        }

        return ViewPaginationProduct.builder()
                .numberOfElements(products.getNumberOfElements())
                .pageNumber(products.getNumber())
                .totalElements(products.getTotalElements())
                .totalPages(products.getTotalPages())
                .product(list)
                .build();
    }

    public CekDataStok cekAllDataReady() {

        List<Product> product = productRepository.findAll();

        Predicate<Product> productPredicate = e -> e.getAmount() < 1;

        boolean match = product.stream()
                        .anyMatch(productPredicate);

        String keadaan = match ? "ada stok product kosong" : "semua product memiliki stok";

        if (match) {
            List<Product> productList = product.stream()
                    .filter(r -> r.getAmount() == 0)
                    .collect(Collectors.toList());

            return CekDataStok.builder()
                    .keterangan(keadaan)
                    .content(productList)
                    .build();
        }

        return CekDataStok.builder()
                .keterangan(keadaan)
                .build();
    }

    public Product getProductById(String id) {

        Product product = productRepository.getOne(id);

        return Product.builder()
                .id(product.getId())
                .nameProduct(product.getNameProduct())
                .amount(product.getAmount())
                .priceProduct(product.getPriceProduct())
                .build();
    }

    public ResponseStatus insertCustomer(Pembeli request) {

        try {
            Pembeli pembeli = new Pembeli();
            pembeli.setName(request.getName());
            pembeli.setCreatedDate(dateCurrentDate());
            pembeli.setEmail(request.getEmail());
            pembeli.setBalance(request.getBalance() == null ? 0 : request.getBalance());

            pembeliRepository.save(pembeli);

            String type = "insert customer";
            return ResponseStatus.builder()
                    .responseCode("00")
                    .responseMessage("Success")
                    .content(type)
                    .build();

        } catch (Exception e) {
            log.error("Exception insertCustomer : ", e);
            return ResponseStatus.builder()
                    .responseCode("01")
                    .responseMessage("Error insertCustomer")
                    .build();
        }

    }

    public ResponseStatus insertEmployee(ProductDto request) {

        try {
            Employee employee = new Employee();
            employee.setName(request.getName());
            employee.setCreatedDate(dateCurrentDate());

            employeeRepository.save(employee);

            String type = "insert employee";
            return ResponseStatus.builder()
                    .responseCode("00")
                    .responseMessage("Success")
                    .content(type)
                    .total("10")
                    .build();

        } catch (Exception e) {
            log.error("Exception insertEmployee : ", e);
            return ResponseStatus.builder()
                    .responseCode("01")
                    .responseMessage("Error insertEmployee")
                    .build();
        }

    }

}
