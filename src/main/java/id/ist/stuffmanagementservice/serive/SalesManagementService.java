package id.ist.stuffmanagementservice.serive;

import freemarker.template.Configuration;
import freemarker.template.Template;
import id.ist.stuffmanagementservice.domain.*;
import id.ist.stuffmanagementservice.dto.ResponseStatus;
import id.ist.stuffmanagementservice.dto.mail.ListProduct;
import id.ist.stuffmanagementservice.dto.mail.MailRequestDto;
import id.ist.stuffmanagementservice.dto.mail.MailResponseDto;
import id.ist.stuffmanagementservice.dto.mail.ResponseMail;
import id.ist.stuffmanagementservice.dto.product.ProductDto;
import id.ist.stuffmanagementservice.dto.receipt.ReceiptTransaction;
import id.ist.stuffmanagementservice.dto.transaction.CekDto;
import id.ist.stuffmanagementservice.dto.transaction.TransactionDto;
import id.ist.stuffmanagementservice.dto.transaction.TransactionRequest;
import id.ist.stuffmanagementservice.repository.*;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;
import org.springframework.util.StringUtils;

import javax.mail.internet.MimeMessage;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

@Log4j2
@Service
public class SalesManagementService {

    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private PembeliRepository pembeliRepository;

    @Autowired
    private StatusTransactionRepository statusTransactionRepository;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private JavaMailSender sender;

    @Autowired
    private Configuration configuration;

    public static Date dateCurrentDate() {
        TimeZone timeZone = TimeZone.getTimeZone("Asia/Jakarta");
        Calendar calendar = Calendar.getInstance(timeZone);
        Date date = calendar.getTime();
        return date;
    }

    public ResponseStatus addTransactionProduct(TransactionRequest request) {

        String statusCode = ""; // 01 -> catch excaption, 02 -> id not fund
        try {

            Pembeli dataPembeli = pembeliRepository.getById(request.getPembeliId());
            List<Product> productList = new ArrayList<>();

            if (dataPembeli != null) {
                log.info("### gagal tranx" + dataPembeli);

//                int benginingBalance = 0;

                TransactionDto tran = TransactionDto.builder()
                        .amount(0L)
                        .build();

                for (TransactionDto data : request.getTransaction()){
                    Product dataProduct = productRepository.getById(data.getProductId());

                    tran = TransactionDto.builder()
//                            .productId(String.valueOf(new BigDecimal((tran.getAmount() + (data.getAmount() * dataProduct.getPriceProduct()))- dataPembeli.getBalance())))
                            .productId(tran.getAmount() + (data.getAmount() * dataProduct.getPriceProduct()) > dataPembeli.getBalance() ? "Y" : "N")
                            .amount(tran.getAmount() + (data.getAmount() * dataProduct.getPriceProduct()))
                            .build();
                }


            for (TransactionDto data : request.getTransaction()){

                if (tran.getProductId().equals("N")) {

                    Product dataProduct = productRepository.getById(data.getProductId());
                    log.info("### data tran : " + tran);

                        Product product = new Product();
                        product.setAmount(dataProduct.getAmount() - data.getAmount());
                        productList.add(product);

                        dataPembeli.setBalance((tran.getAmount() + (data.getAmount() * dataProduct.getPriceProduct()))- dataPembeli.getBalance());

                        log.info("### data pembeli " + dataPembeli.getBalance());
                        log.info("### save trans");

                    } else {
                        log.info("### gagal tranx");
                    }
            }

//                //save product
//                productRepository.saveAll(productList);
//                //save PEMBELI
//                log.info("### data pembeli1 " + dataPembeli.getBalance());
//                pembeliRepository.save(dataPembeli);


                log.info("### data tran : " + tran);
            }
            else {
                statusCode = "02";
            }

            //save product
            productRepository.saveAll(productList);
            //save PEMBELI
            log.info("### data pembeli1 " + dataPembeli.getBalance());
            pembeliRepository.save(dataPembeli);

        } catch (Exception e) {
            statusCode = "01";
        }
        return ResponseStatus.builder().build();
    }
    public ResponseStatus addTransactionProductv1(TransactionRequest request) {

        MimeMessage message = sender.createMimeMessage();
//        List<ResponseMail> responseMails = new ArrayList<>();
        List<ListProduct> listProducts = new ArrayList<>();
        String statusCode = ""; // 01 -> catch excaption, 02 -> id not fund

        Pembeli dataPembeli = pembeliRepository.getById(request.getPembeliId());
        List<Product> productList = new ArrayList<>();
        List<ProductDto> dataproduct = new ArrayList<>();
        List<Transaction> transactions = new ArrayList<>();

        DateFormat dayhourminute = new SimpleDateFormat("ddHHmm");
        DateFormat mountyear = new SimpleDateFormat("MM/yyyy");

        if (dataPembeli == null) {
            return ResponseStatus.builder()
                    .responseCode("01")
                    .responseMessage("Id Pembeli Tidak Ada")
                    .build();
        }

        try {

            TransactionDto tran = TransactionDto.builder()
                    .amount(0L)
                    .build();

            for (TransactionDto data : request.getTransaction()) {

                Product product = productRepository.getById(data.getProductId());

                tran = TransactionDto.builder()
                        .amount(tran.getAmount() + (data.getAmount() * product.getPriceProduct()))
                        .build();

                long productData = product.getAmount() - data.getAmount();

                if (productData < 0) {

                    CekDto cekDto = CekDto.builder()
                            .begining(String.valueOf(product.getAmount()))
                            .ending(String.valueOf(productData))
                            .amountTransaction(String.valueOf(data.getAmount()))
                            .build();

                    return ResponseStatus.builder()
                            .responseCode("01")
                            .responseMessage("Stok Tidak Cukup / habis")
                            .content(cekDto)
                            .build();
                }

                product.setAmount(product.getAmount() - data.getAmount());
                productList.add(product);

                Transaction transaction = new Transaction();
                transaction.setCreatedDate(dateCurrentDate());
                transaction.setStatusId(dayhourminute.format(dateCurrentDate()).concat("/TRANSACTION/")
                        .concat(mountyear.format(dateCurrentDate())));
                transaction.setEmployeeId(request.getEmployeeId());
                transaction.setProductId(data.getProductId());
                transaction.setPembeliId(request.getPembeliId());
                transaction.setTransactionId("00");
                log.info("### data transaction : " + transaction);
                transactions.add(transaction);

                ProductDto productDto = ProductDto.builder()
                        .name(product.getNameProduct())
                        .price(product.getPriceProduct())
                        .amount(data.getAmount())
                        .total(data.getAmount() * product.getPriceProduct())
                        .build();
                dataproduct.add(productDto);

                ListProduct listProduct = ListProduct.builder()
                        .product(product.getNameProduct().concat(" x ").concat(String.valueOf(data.getAmount()).concat(" @").concat(String.valueOf(product.getPriceProduct()))))
                        .build();
                listProducts.add(listProduct);
            }

            StatusTransaction statusTran = new StatusTransaction();

            statusTran.setId(dayhourminute.format(dateCurrentDate()).concat("/TRANSACTION/")
                    .concat(mountyear.format(dateCurrentDate())));
            statusTran.setStatus("00");
            statusTran.setDescription("SUCCESS");
            statusTran.setCreatedDate(dateCurrentDate());
            statusTran.setPriceProduct(tran.getAmount());
            statusTran.setBeginingBalance(dataPembeli.getBalance());
            statusTran.setEndingBalance(dataPembeli.getBalance() - tran.getAmount());

            long balance = dataPembeli.getBalance() - tran.getAmount();
            if (balance < 0) {

                CekDto cekDto = CekDto.builder()
                        .begining(String.valueOf(dataPembeli.getBalance()))
                        .ending(String.valueOf(balance))
                        .amountTransaction(String.valueOf(tran.getAmount()))
                        .build();

                return ResponseStatus.builder()
                        .responseCode("01")
                        .responseMessage("Saldo Tidak Cukup")
                        .content(cekDto)
                        .build();
            }

            dataPembeli.setBalance(dataPembeli.getBalance() - tran.getAmount());

            log.info("### data pemeberli" + dataPembeli.getBalance());
            log.info("### data tran : " + tran);

//            productRepository.saveAll(productList);
//            pembeliRepository.save(dataPembeli);
//            transactionRepository.saveAll(transactions);
//            statusTransactionRepository.save(statusTran);

            Employee employee = employeeRepository.getById(request.getEmployeeId());

            ReceiptTransaction receiptTransaction = ReceiptTransaction.builder()
                    .idTransaction(statusTran.getId())
                    .date(String.valueOf(dateCurrentDate()))
                    .pegawai(employee.getName())
                    .pembeli(dataPembeli.getName())
                    .listProduct(dataproduct)
                    .build();

            ResponseMail responseMail = ResponseMail.builder()
                    .listProducts(listProducts)
                    .build();

            String dataMail = String.valueOf(responseMail.getListProducts());
            Map<String, Object> model = new HashMap<>();
            String regex = "\\[|\\]";
            String regex1 = "\\)";
            model.put("name", dataPembeli.getName());
            model.put("product", dataMail.replaceAll(regex, "")
                    .replaceAll("ListProduct[(]product=", "<br>").replaceAll(regex1,""));

            model.put("tranId", statusTran.getId());
            model.put("dateTime", receiptTransaction.getDate());
            model.put("total", tran.getAmount());

            if (!receiptTransaction.equals("null")) {
                // set mediaType
                MimeMessageHelper helper = new MimeMessageHelper(message, MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
                        StandardCharsets.UTF_8.name());
                Template t = configuration.getTemplate("email-template1.ftl");
                String html = FreeMarkerTemplateUtils.processTemplateIntoString(t, model);

                helper.setTo(dataPembeli.getEmail());
                log.info("### helper email to : " + dataPembeli.getEmail());

                helper.setSubject("Bukti Transaction IST");
                helper.setFrom(employee.getEmail());
                log.info("### helper email from : " + employee.getEmail());
                helper.setText(html);
                sender.send(message);
            }




            return ResponseStatus.builder()
                    .responseCode("00")
                    .responseMessage("Success")
                    .content(receiptTransaction)
                    .build();

        } catch (Exception e) {
            return ResponseStatus.builder()
                    .responseCode("01")
                    .responseMessage("Error Transaction")
                    .build();
        }
    }

    public ResponseStatus inquiryTransaction(TransactionRequest request) {

        try {

            if (request.getTransaction().size() == 0) {
                return ResponseStatus.builder()
                        .responseCode("01")
                        .responseMessage("Error Transaction")
                        .build();
            }

            if (StringUtils.isEmpty(request.getPembeliId())) {
                return ResponseStatus.builder()
                        .responseCode("01")
                        .responseMessage("Customer null")
                        .build();
            }

            return ResponseStatus.builder()
                    .responseCode("00")
                    .responseMessage("Success")
                    .build();
        } catch (Exception e) {
            return ResponseStatus.builder()
                    .responseCode("01")
                    .responseMessage("Error Transaction")
                    .build();
        }
    }
}
