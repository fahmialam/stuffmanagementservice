package id.ist.stuffmanagementservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StuffManagementServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(StuffManagementServiceApplication.class, args);
    }

}
