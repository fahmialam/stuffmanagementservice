package id.ist.stuffmanagementservice.utils;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class Utils {

    public static Date dateCurrentDate() {
        TimeZone timeZone = TimeZone.getTimeZone("Asia/Jakarta");
        Calendar calendar = Calendar.getInstance(timeZone);
        Date date = calendar.getTime();
        return date;
    }
}
