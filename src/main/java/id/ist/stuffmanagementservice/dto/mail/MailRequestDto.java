package id.ist.stuffmanagementservice.dto.mail;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MailRequestDto {

    private String name;
    private String from;
    private String to;
    private String subject;
}
