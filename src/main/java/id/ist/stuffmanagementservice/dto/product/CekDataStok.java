package id.ist.stuffmanagementservice.dto.product;

import id.ist.stuffmanagementservice.domain.Product;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CekDataStok {

    private String keterangan;
    private Object content;
}
