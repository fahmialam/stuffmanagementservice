package id.ist.stuffmanagementservice.dto.product;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ViewPaginationProduct {

    private int totalPages;
    private long totalElements;
    private int numberOfElements;
    private int pageNumber;
    private List<ProductDto> product;
}
