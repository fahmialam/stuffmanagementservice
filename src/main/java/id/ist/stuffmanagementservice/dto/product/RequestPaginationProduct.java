package id.ist.stuffmanagementservice.dto.product;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class RequestPaginationProduct {
    private String type;
    private String sortType;
    private String sortBy;
    private int pageNumber;
    private int dataPerPage;
}
