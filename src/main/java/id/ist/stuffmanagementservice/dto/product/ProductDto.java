package id.ist.stuffmanagementservice.dto.product;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class ProductDto {

    private String id;
    private String name;
    private Long amount;
    private Long price;
    private Long total;
}
