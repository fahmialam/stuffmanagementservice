package id.ist.stuffmanagementservice.dto.transaction;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CekDto {

    private String begining;
    private String ending;
    private String amountTransaction;
}
