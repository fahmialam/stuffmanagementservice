package id.ist.stuffmanagementservice.dto.receipt;

import id.ist.stuffmanagementservice.dto.product.ProductDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class ReceiptTransaction {

    private String pegawai;
    private String pembeli;
    private String date;
    private String idTransaction;
    private List<ProductDto> listProduct;
}
