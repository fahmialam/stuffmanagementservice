package id.ist.stuffmanagementservice.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ResponseStatus {

    private String responseCode;
    private String responseMessage;
    private Object content;
    private String total;
}
