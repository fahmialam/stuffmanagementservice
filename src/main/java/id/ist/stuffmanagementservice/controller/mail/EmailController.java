package id.ist.stuffmanagementservice.controller.mail;

import id.ist.stuffmanagementservice.dto.mail.MailRequestDto;
import id.ist.stuffmanagementservice.dto.mail.MailResponseDto;
import id.ist.stuffmanagementservice.serive.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
public class EmailController {

    @Autowired
    private NotificationService notificationService;

    @PostMapping("/sendingEmail")
    public MailResponseDto sendEmail(@RequestBody MailRequestDto request) {
        Map<String, Object> model = new HashMap<>();
        model.put("Name", request.getName());
        model.put("location", "Bangalore,India");
        return notificationService.sendEmail(request, model);

    }
}
