package id.ist.stuffmanagementservice.controller;

import id.ist.stuffmanagementservice.domain.Pembeli;
import id.ist.stuffmanagementservice.domain.Product;
import id.ist.stuffmanagementservice.dto.ResponseStatus;
import id.ist.stuffmanagementservice.dto.product.*;
import id.ist.stuffmanagementservice.dto.transaction.TransactionRequest;
import id.ist.stuffmanagementservice.repository.ProductRepository;
import id.ist.stuffmanagementservice.serive.SalesManagementService;
import id.ist.stuffmanagementservice.serive.StuffManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

import static id.ist.stuffmanagementservice.controller.PathConstants.*;

@RestController
public class ProductConttroller {

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    public ProductConttroller(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Autowired
    private StuffManagementService stuffManagementService;

    @Autowired
    private SalesManagementService salesManagementService;

    @GetMapping(PRODUCT)
    public Page<Product> findAll(Pageable pageable) {
        return productRepository.findAll(pageable);
    }

    @PostMapping(UPDATE_PRODUCT)
    public ResponseStatus updateProduct(@RequestBody ListDataProductDto request) {
        return stuffManagementService.saveOrUpdateProductv1(request);
    }

    @PostMapping(INSERT_PRODUCT)
    public ResponseStatus insertProduct(@RequestBody ListDataProductDto request) {
        return stuffManagementService.InsertProduct(request);
    }

    @PostMapping(UPDATE_AMOUNT_PRODUCT)
    public ResponseStatus updateAmountProduct(@RequestBody ProductDto request) {
        return stuffManagementService.InsertAmountProduct(request);
    }

    @PostMapping(PAYMENT_TRANSACTION)
    public ResponseStatus newTransactionProduct(@RequestBody TransactionRequest request) {
        return salesManagementService.addTransactionProductv1(request);
    }

    @PostMapping(INQUIRY_TRANSACTION)
    public ResponseStatus inquiryTransaction(@RequestBody TransactionRequest request) {
        return salesManagementService.inquiryTransaction(request);
    }

    @PostMapping(VIEW_PRODUCT)
    public ViewPaginationProduct getListProduct(@RequestBody RequestPaginationProduct request) {
        return stuffManagementService.getListProduct(request);
    }

    @GetMapping(GET_PRODUCT)
    public Product getProduct(@RequestParam("id") String id) {
//        return productRepository.getOne(request);
        return stuffManagementService.getProductById(id);
    }

    @PostMapping(INSERT_CUSTOMER)
    public ResponseStatus insertCustomer(@RequestBody Pembeli request) {
        return stuffManagementService.insertCustomer(request);
    }

    @PostMapping(INSERT_EMPLOYEE)
    public ResponseStatus insertEmploye(@RequestBody ProductDto request) {
        return stuffManagementService.insertEmployee(request);
    }

    @PostMapping(CEK_PRODUCT)
    public CekDataStok cekProduct() {
        return stuffManagementService.cekAllDataReady();
    }
}
