package id.ist.stuffmanagementservice.controller;

public class PathConstants {

    public static final String PRODUCT = "/product";
    public static final String UPDATE_PRODUCT = PRODUCT + "/update";
    public static final String CEK_PRODUCT = PRODUCT + "/cek";
    public static final String VIEW_PRODUCT = PRODUCT + "/view";
    public static final String GET_PRODUCT = PRODUCT + "/get";
    public static final String UPDATE_AMOUNT_PRODUCT = PRODUCT + "/update-amount";
    public static final String INSERT_PRODUCT = PRODUCT + "/insert";
    public static final String TRANSACTION = "/transaction";
    public static final String PAYMENT_TRANSACTION = TRANSACTION + "/payment";
    public static final String INQUIRY_TRANSACTION = TRANSACTION + "/inquiry";
    public static final String CUSTOMER = "/customer";
    public static final String INSERT_CUSTOMER = CUSTOMER + "/insert";
    public static final String EMPLOYEE = "/employee";
    public static final String INSERT_EMPLOYEE = EMPLOYEE + "/insert";

}
