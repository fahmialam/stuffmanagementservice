package id.ist.stuffmanagementservice.repository;

import id.ist.stuffmanagementservice.domain.Pembeli;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface PembeliRepository extends JpaRepository<Pembeli, String> {

    @Query("SELECT a from Pembeli a where a.id = :id")
    Pembeli getById (String id);
}
