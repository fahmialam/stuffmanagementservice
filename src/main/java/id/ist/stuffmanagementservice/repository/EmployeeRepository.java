package id.ist.stuffmanagementservice.repository;

import id.ist.stuffmanagementservice.domain.Employee;
import id.ist.stuffmanagementservice.domain.Pembeli;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface EmployeeRepository extends JpaRepository<Employee, String> {

    @Query("SELECT a from Employee a where a.id = :id")
    Employee getById (String id);
}
