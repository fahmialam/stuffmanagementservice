package id.ist.stuffmanagementservice.repository;

import id.ist.stuffmanagementservice.domain.StatusTransaction;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StatusTransactionRepository extends JpaRepository<StatusTransaction, String> {
}
