package id.ist.stuffmanagementservice.repository;

import id.ist.stuffmanagementservice.domain.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TransactionRepository extends JpaRepository<Transaction, String> {
}
