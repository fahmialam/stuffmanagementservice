package id.ist.stuffmanagementservice.repository;

import id.ist.stuffmanagementservice.domain.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends JpaRepository<Product, String> {

    Page<Product> findAll(Pageable pageable);

    @Query("SELECT a from Product a where a.id = :id")
    Product getById (String id);

}
