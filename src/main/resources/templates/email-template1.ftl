<table border="0" width="500px" cellspacing="0" cellpadding="0">
    <tbody>
    <tr>
        <td class="text-decoration">Bukti Transaksi IST</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td class="text-decoration" valign="center">Kode Resi : ${tranId}</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <table border="0" width="100%" cellspacing="0" cellpadding="0">
                <tbody>
                <tr>
                    <td class="text-decoration" valign="center">Tanggal/Waktu:</td>
                </tr>
                <tr>
                    <td class="text-decoration" valign="center">${dateTime}</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="text-decoration" valign="center">Product :</td>
                </tr>
                <tr>
                    <td class="text-decoration" valign="center">${product}</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="text-decoration" valign="center">Total : ${total}</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="text-decoration" valign="center">Payment Method : Transfer</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="text-decoration" valign="center">Customer Name : ${name}</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <td class="text-decoration">terima kasih.</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    </tbody>
</table>
